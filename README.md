# uTipOBS

Script Node.js réceptionnant les alertes de dons et d'achats du service [uTip](https://utip.io/) (non officiel) et déclenchant l'affichage de sources sur OBS en fonction du montant du don ou du produit acheté.

## Pré-requis

- Le logiciel [OBS](https://obsproject.com/).
- Le plugin [obs-websocket](https://github.com/Palakis/obs-websocket).
- [Node.js](https://nodejs.org) en version 13 ou supérieure.

## Installation

- [Télécharger le projet](https://gitlab.com/Desmu/utipobs/-/archive/master/utipobs-master.zip).
- Dans le dossier du projet, installer les dépendances externes avec l'une des options suivantes :
  * Exécuter le fichier `install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles).
  * Exécuter la commande `npm install --prod` dans le dossier du programme depuis le terminal pour installer les librairies utilisées, et `npm install -g pm2` pour installer le gestionnaire de processus permettant de garder le script actif.
- Renommer le fichier `configuration.example.json` en `configuration.json` et en modifier les paramètres (voir paragraphe `Configuration`) via un éditeur de texte autre que le Bloc-Notes (Atom, Notepad++, Visual Studio, Sublime Text, ...).
- Dans le menu `Outils > Scripts` d'OBS, ajouter le script `start.lua`, puis spécifier le chemin du fichier `utipobs.js` dans le champ venant d'apparaître.

## Configuration

- `obswsAddress` : adresse utilisée par OBS Websocket (visible dans le menu d'OBS `Outils > Paramètres du serveur WebSockets`, `localhost:4444` par défaut).
- `obswsPassword` : mot de passe utilisé par OBS WebSocket (visible dans le menu d'OBS `Outils > Paramètres du serveur WebSockets`, laissez vide si l'authentification n'est pas activée).
- `utipOverlayId` : identifiant fourni par uTip pour votre overlay (visible dans le menu de uTip `Overlay Live > Alertes dons et achats`, inclus dans l'URL `https://utip.io/video-overlay/IDENTIFIANT?type=msg`).
- `tip` : liste des configurations à appliquer en cas d'alerte de don, chaque configuration est nommée par la somme à partir de laquelle elle est enclenchée (par exemple, la configuration `10` est appliquée pour tout don supérieur ou égal à 10 euros, `01` pour tout don supérieur ou égal à 1 euro, ...)
- `product` : liste des configurations à appliquer en cas d'alerte d'achat de produit, chaque configuration est nommée par le produit concerné.
- `plugins` : liste des informations mises à disposition par uTip, changer le paramètre d'une information de `false` à `true` permet d'activer sa synchronisation dans le script (le dossier `plugins` contient des fichiers textes relatifs à chaque information qui seront mis à jour si leur synchronisation est activée, passer un fichier texte comme source texte dans OBS permet à son contenu d'être affiché et changé dès que l'information est reçue).

Chaque configuration de don et de produit comprend les champs suivants :
- `sceneName` : nom de la scène OBS contenant les sources à déclencher.
- `textSourceName` : (facultatif) nom de la source texte OBS contenant le texte de remerciement à afficher.
- `mediaSourceName` : (facultatif) nom de la source image ou vidéo OBS contenant le média à afficher.
- `messageSourceName` : (facultatif) nom de la source texte OBS contenant le message envoyé par la personne ayant effectué le don ou l'achat.
- `textSourceContent` : texte de remerciement à afficher, les variables `$pseudo` (pseudonyme de la personne), `$amount` (somme d'argent donnée), `$currency` (symbole de la monnaie utilisée) et `$product` (nom du produit acheté) peuvent y être ajoutées, ajouter le symbole `\n` permet d'insérer un saut de ligne.
- `seconds` : nombre de secondes s'écoulant avant que l'alerte ne disparaisse de l'écran.

## Plugins disponibles

Le dossier `plugins` comporte des fichiers qui seront automatiquement modifiés par les alertes uTip avec les valeurs suivantes :
- `balance` : Solde
- `goal-amount` : Montant de l'objectif
- `goal-desc` : Description de l'objectif
- `goal-number-of-tiprs` : Nombre de TipRs pour l'objectif
- `last-donator` : Dernier donateur
- `last-donation-amount` : Montant du dernier don
- `last-recurring-donation-amount` : Montant du dernier don récurrent
- `last-recurring-donator` : Donateur du dernier don récurrent
- `best-tipr-ever` : Meilleur TipR
- `best-tipr-amount-ever` : Montant rapporté par le meilleur TipR
- `last-buyer` : Dernier acheteur
- `last-bought-product` : Dernier produit acheté
- `last-bought-product-amount` : Prix du dernier produit acheté
- `last-firetipr` : Dernier FireTipR
- `oldest-firetip` : Plus ancien FireTipR
- `higher-return-firetipr-donator` : FireTipR ayant rapporté le plus
- `higher-return-firetipr-amount` : Montant rapporté par le FireTipR ayant rapporté le plus
- `largest-donation-amount-ever` : Montant du plus gros don
- `largest-donation-donator-ever` : Donateur du plus gros don
- `largest-donation-amount-session` : Montant du plus gros don de la session
- `largest-donation-donator-session` : Donateur du plus gros don de la session
- `largest-recurring-donation-amount` : Montant du plus gros don récurrent
- `largest-recurring-donation-donator` : Donateur du plus gros don récurrent
- `largest-buy-amount-ever` : Montant du plus gros achat
- `largest-buy-product-ever` : Produit du plus gros achat
- `largest-buy-donator-ever` : Donateur du plus gros achat
- `largest-buy-donator-session` : Donateur du plus gros achat de la session
- `largest-buy-amount-session` : Montant du plus gros achat de la session
- `largest-buy-product-session` : Produit acheté lors du plus gros achat de la session
- `last-30-days-income` : Revenus sur les 30 derniers jours
- `current-month-income` : Revenus depuis le début du mois
- `last-created-product` : Dernier produit crée
- `last-created-product-price` : Prix du dernier produit crée
- `number-of-subscribers` : Nombre d'abonnés sur uTip
- `last-ads-message` : Dernier message reçu suite à un visionnage de pub

## Librairies utilisées

- [eventsource](https://github.com/EventSource/eventsource) pour la communication avec uTip.
- [obs-websocket-js](https://github.com/haganbmj/obs-websocket-js) pour la liaison à OBSWebSocket.
- [pm2](https://github.com/Unitech/pm2) pour le lancement et l'arrêt du processus.
