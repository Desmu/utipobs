obs = obslua
script_path = ""
loaded = false

function start_script()
  if (script_path ~= "") and (loaded == false) then
    loaded = true
    os.execute("pm2 start " .. script_path)
  end
end

function stop_script()
  if (loaded == true) then
    loaded = false
    os.execute("pm2 stop utipobs && pm2 flush utipobs && pm2 delete utipobs && pm2 kill")
  end
end

function script_description()
  return "Script de démarrage de uTipOBS"
end

function script_properties()
  props = obs.obs_properties_create()
  obs.obs_properties_add_path(props, "script_path", "utipobs.js", obs.OBS_PATH_FILE, "(*.js)", NULL)
  return props
end

function script_load(settings)
  start_script()
end

function script_update(settings)
  script_path = obs.obs_data_get_string(settings, "script_path")
  stop_script()
  start_script()
end

function script_unload()
  stop_script()
end
