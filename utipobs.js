import fs from "fs";
import https from "https";
import path from "path";
import url from "url";
import EventSource from "eventsource";
import OBSWebSocket from "obs-websocket-js";

const obs = new OBSWebSocket();
const configuration = JSON.parse(fs.readFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), "./configuration.json"), "utf8"));

function debug (messages, error) {
    if (process.argv.indexOf("--debug") !== -1) {
        console.log((new Date()).toISOString());
        if (error) {
            console.error(error);
        }
        messages.forEach((message) => {
            console.log(message);
        });
    }
}

function setTextSource (sceneName, sourceName, text, visible) {
    obs.send("SetSourceSettings", {
        sourceName,
        "sourceSettings": {
            text
        }
    }).catch((error) => {
        debug([
            "setTextSource SetSourceSettings",
            `sceneName : ${sceneName}`,
            `sourceName : ${sourceName}`,
            `text : ${text}`,
            `visible : ${visible}`
        ], error);
    });
    obs.send("SetSceneItemProperties", {
        "scene-name": sceneName,
        "item": sourceName,
        visible
    }).catch((error) => {
        debug([
            "setTextSource SetSceneItemProperties",
            `sceneName : ${sceneName}`,
            `sourceName : ${sourceName}`,
            `text : ${text}`,
            `visible : ${visible}`
        ], error);
    });
}

function setMediaSource (sceneName, sourceName, visible) {
    obs.send("SetSceneItemProperties", {
        "scene-name": sceneName,
        "item": sourceName,
        visible
    }).catch((error) => {
        debug([
            "setMediaSource SetSceneItemProperties",
            `sceneName : ${sceneName}`,
            `sourceName : ${sourceName}`,
            `visible : ${visible}`
        ], error);
    });
}

function primaryEventSource () {
    const evtSource = new EventSource(`https://mbbvrt.stackhero-network.com/.well-known/mercure?topic=overlay/message/${configuration.utipOverlayId}/bil`);
    evtSource.onopen = () => {
        debug([
            "uTip OK"
        ]);
    };
    evtSource.onmessage = (e) => {
        const data = JSON.parse(e.data);
        debug([
            data
        ]);
        debug([
            data
        ]);
        obs.connect({"address": configuration.obswsAddress, "password": configuration.obswsPassword}).then(() => {
            debug([
                "OBS OK"
            ]);
            let sourceSelected = null;
            let text = "";
            switch (data.type) {
            case "tip":
                Object.keys(configuration.tip).some((price) => {
                    if (data.amount >= parseInt(price, 10)) {
                        sourceSelected = configuration[data.type][price];
                        return true;
                    }
                    return false;
                });
                break;
            case "product":
                Object.keys(configuration.product).some((product) => {
                    if (data.product.name === product) {
                        sourceSelected = configuration[data.type][product];
                        return true;
                    }
                    return false;
                });
                break;
            default:
                break;
            }
            if (sourceSelected.textSourceContent) {
                text = sourceSelected.textSourceContent.replace("$pseudo", data.pseudo).replace("$amount", (data.amount / 100).toFixed(2)).replace("$currency", data.currency);
                if (data.type === "product") {
                    text = text.replace("$product", data.product.name);
                }
            }
            if (sourceSelected.textSourceName) {
                setTextSource(sourceSelected.sceneName, sourceSelected.textSourceName, text, true);
            }
            if (sourceSelected.mediaSourceName) {
                setMediaSource(sourceSelected.sceneName, sourceSelected.mediaSourceName, true);
            }
            if (sourceSelected.messageSourceName) {
                setTextSource(sourceSelected.sceneName, sourceSelected.messageSourceName, data.text, true);
            }
            setTimeout(() => {
                if (sourceSelected.messageSourceName) {
                    setTextSource(sourceSelected.sceneName, sourceSelected.messageSourceName, "", false);
                }
                if (sourceSelected.mediaSourceName) {
                    setMediaSource(sourceSelected.sceneName, sourceSelected.mediaSourceName, false);
                }
                if (sourceSelected.textSourceName) {
                    setTextSource(sourceSelected.sceneName, sourceSelected.textSourceName, "", false);
                }
                obs.disconnect();
            }, sourceSelected.seconds * 1000);
        }).catch(() => {
            debug([
                "OBS KO"
            ]);
        });
    };
    evtSource.onerror = (error) => {
        debug([
            "uTip KO"
        ], error);
    };
}

function secondaryEventSources () {
    Object.entries(configuration.plugins).forEach(([plugin, isEnabled]) => {
        if (isEnabled) {
            const options = {
                "hostname": "utip.io",
                "port": 443,
                "path": `/video-overlay/${configuration.utipOverlayId}/stream?type=tag&tag=${plugin}`,
                "method": "GET"
            };
            const req = https.request(options, (res) => {
                res.setEncoding("utf8");
                res.on("data", (data) => {
                    if (data.indexOf("event: tag") !== -1) {
                        const realData = data.split("event: tag")[1].replace("data:", "").trim();
                        fs.writeFileSync(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), `./plugins/${plugin}.txt`), realData);
                    }
                });
            });
            req.on("error", (error) => {
                debug([
                    `request to ${plugin} error`
                ], error);
            });
            req.end();
        }
    });
}

primaryEventSource();
secondaryEventSources();
