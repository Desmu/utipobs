import child_process from "child_process";
import path from "path";
import url from "url";

process.chdir(path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), "."));

child_process.exec("npm install --prod", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});

child_process.exec("npm install -g pm2", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});
